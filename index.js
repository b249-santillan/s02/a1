//  QUIZ SECTION
	/*
		1. What is the term given to unorganized code that's very hard to work
		with?

			ANSWER: spaghetti code

		2. How are object literals written in JS?

			ANSWER: curly braces {}

		3. What do you call the concept of organizing information and functionality to belong to an object?

			ANSWER: Object Oriented Programming

		4. If studentOne has a method named enroll(), how would you invoke it?

			ANSWER: studentOne.enroll()

		5. True or False: Objects can have objects as properties.

			ANSWER: True

		6. What is the syntax in creating key-value pairs?

			ANSWER: keyA: valueA,
					keyB: valueB

		7. True or False: A method can have no parameters and still work.

			ANSWER: True

		8. True or False: Arrays can have objects as elements.

			ANSWER: True

		9. True or False: Arrays are objects.

			ANSWER: True

		10. True or False: Objects can have arrays as properties.

			ANSWER: True

	*/

// FUNCTION CODING SECTION
	
	/*
		// create student 1
		let studentOneName = "John";
		let studentOneEmail = 'john@mail.com';
		let studentOneGrades = [89, 84, 78, 88];

		// create student 2
		let studentTwoName = "Joe";
		let studentTwoEmail = 'joe@mail.com';
		let studentTwoGrades = [78, 82, 79, 85];

		// create student 3
		let studentThreeName = "Jane";
		let studentThreeEmail = 'jane@mail.com';
		let studentThreeGrades = [87, 89, 91, 93];

		// create student 4
		let studentFourName = "Jessie";
		let studentFourEmail = 'jessie@mail.com';
		let studentFourGrades = [91, 89, 92, 93];
	*/
	
	//student 1
	 let studentOne =
	 {
	 	name: 'John',
	 	email: 'john@mail.com',
	 	grades: [89, 84, 78, 88],
	 	
	 	login()	{
		 	console.log	(`${this.email} has logged in`)
		},

		logout() {
		 	console.log(`${this.email} has logged out`)	
		},

		listGrades() {
			console.log(`${this.name}'s quarterly grades are: ${this.grades}`)	
		},

		 
		computeAve () {
			
			let sum = 0;
			this.grades.forEach(grade => sum += grade);
			return sum/this.grades.length;
		},

		willPass () {
			return this.computeAve() >= 85 ? true : false ;
		},


		willPassWithHonors() {
			if (this.computeAve() >= 90 && this.willPass())
			{
				return true
			};

			if (this.computeAve() < 90 && this.willPass())
			{
				return false
			};
			if (!this.willPass())
			{
				return undefined
			}
		}

	 }

	//student 2
	 let studentTwo =
	 {
	 	name: 'Joe',
	 	email: 'joe@mail.com',
	 	grades: [78, 82, 79, 85],
	 	
	 	login()	{
		 	console.log	(`${this.email} has logged in`)
		},

		logout() {
		 	console.log(`${this.email} has logged out`)	
		},

		listGrades() {
			console.log(`${this.name}'s quarterly grades are: ${this.grades}`)	
		},

		 
		computeAve () {
			
			let sum = 0;
			this.grades.forEach(grade => sum += grade);
			return sum/this.grades.length;
		},

		willPass () {
			return this.computeAve() >= 85 ? true : false ;
		},


		willPassWithHonors() {
			if (this.computeAve() >= 90 && this.willPass())
			{
				return true
			};

			if (this.computeAve() < 90 && this.willPass())
			{
				return false
			};
			if (!this.willPass())
			{
				return undefined
			}
		}

	 }

	//student 3
	 let studentThree =
	 {
	 	name: 'Jane',
	 	email: 'jane@mail.com',
	 	grades: [87, 89, 91, 93],
	 	
	 	login()	{
		 	console.log	(`${this.email} has logged in`)
		},

		logout() {
		 	console.log(`${this.email} has logged out`)	
		},

		listGrades() {
			console.log(`${this.name}'s quarterly grades are: ${this.grades}`)	
		},

		 
		computeAve () {
			
			let sum = 0;
			this.grades.forEach(grade => sum += grade);
			return sum/this.grades.length;
			
		},

		willPass () {
			return this.computeAve() >= 85 ? true : false ;
		},


		willPassWithHonors() {
			if (this.computeAve() >= 90 && this.willPass())
			{
				return true
			};

			if (this.computeAve() < 90 && this.willPass())
			{
				return false
			};
			if (!this.willPass())
			{
				return undefined
			}
		}

	 }

	 //student 4
	  let studentFour =
	  {
	  	name: 'Jessie',
	  	email: 'jessie@mail.com',
	  	grades: [91, 89, 92, 93],
	  	
	  	login()	{
	 	 	console.log	(`${this.email} has logged in`)
	 	},

	 	logout() {
	 	 	console.log(`${this.email} has logged out`)	
	 	},

	 	listGrades() {
	 		console.log(`${this.name}'s quarterly grades are: ${this.grades}`)	
	 	},

	 	 
	 	computeAve () {
	 		
	 		let sum = 0;
	 		this.grades.forEach(grade => sum += grade);
	 		return sum/this.grades.length;
	 		
	 	},

	 	willPass () {
	 		return this.computeAve() >= 85 ? true : false ;
	 	},


	 	willPassWithHonors() {
	 		if (this.computeAve() >= 90 && this.willPass())
	 		{
	 			return true
	 		};

	 		if (this.computeAve() < 90 && this.willPass())
	 		{
	 			return false
	 		};
	 		if (!this.willPass())
	 		{
	 			return undefined
	 		}
	 	}

	  }

	// for student 2
	console.log(`Student Two`)

	console.log("Calling studentTwo");
	console.log(studentTwo);

	console.log("studentTwo.computeAve():");
	console.log(studentTwo.computeAve());

	console.log("studentTwo.willPass():");
	console.log(studentTwo.willPass());

	console.log("studentTwo.willPassWithHonors():");
	console.log(studentTwo.willPassWithHonors());

	// for student 3
	console.log(`Student Three`)

	console.log("Calling studentThree");
	console.log(studentThree);

	console.log("studentThree.computeAve():");
	console.log(studentThree.computeAve());

	console.log("studentThree.willPass():");
	console.log(studentThree.willPass());

	console.log("studentThree.willPassWithHonors():");
	console.log(studentThree.willPassWithHonors());

	// for student 4
	console.log(`Student Four`)

	console.log("Calling studentFour");
	console.log(studentFour);

	console.log("studentFour.computeAve():");
	console.log(studentFour.computeAve());

	console.log("studentFour.willPass():");
	console.log(studentFour.willPass());

	console.log("studentFour.willPassWithHonors():");
	console.log(studentFour.willPassWithHonors());

	/*
		5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.

	*/

	let classOf1A = 
	{
		students: [studentOne, studentTwo, studentThree, studentFour],

		/*
			6. Create a method for the object classOf1A named countHonorStudents()
			that will return the number of honor students.

		*/

		countHonorStudents() {
			let honorStudents = 0;
			this.students.forEach(student => 
				{
					if (student.willPassWithHonors() === true)
						{honorStudents += 1}
				})
			return honorStudents

		},

		/*
			7. Create a method for the object classOf1A named honorsPercentage()
			that will return the % of honor students from the batch's total number of
			students.

		*/

		honorsPercentage() {
			return (this.countHonorStudents()/this.students.length)*100
		}
	}

	console.log(classOf1A.students);

	console.log(`classOf1A.countHonorStudents()`)
	console.log(classOf1A.countHonorStudents())


	console.log(`classOf1A.honorsPercentage()`)
	console.log(classOf1A.honorsPercentage())

